# Operation Cybersnare

Operation Cybersnare was a United States Secret Service operation in 1995 targeted at computer hackers.

In January 1995, the Secret Service set up an undercover bulletin board system (called "Celco 51") in Bergen County, New Jersey. [[Cybersnare Sting Arrest]][[Operation Cybersnare]] This was the first undercover Internet sting of its kind. With the help of an undercover informant, they advertised the bulletin board across the Internet. The topics for discussion were cellular telephone cloning and computer hacking.[14]

In September, twelve raids across the country resulted in the arrest of six hackers. From a press release dated September 11, 1995, those arrested were: [[Cybersnare Sting Arrest]][[Operation Cybersnare]]

* Richard Lacap, of Katy, Texas, who used the computer alias of "Chillin" and Kevin Watkins, of Houston, Texas, who used the computer alias of "Led". Lacap and Watkins were charged by criminal complaint with conspiring to break into the computer system of an Oregon Cellular Telephone company.
* Jeremy Cushing, of Huntington Beach, California, who used the computer alias of "Alpha Bits", was charged with trafficking in cloned cellular telephone equipment and stolen access devices used to program cellular telephones.
* Frank Natoli, of Brooklyn, New York, used the computer alias of "Mmind". He was charged with trafficking in stolen access devices used to program cellular telephones.
* Al Bradford, of Detroit, Michigan, who used the computer alias of "Cellfone", was charged with trafficking in unauthorized access devices used to program cellular telephones.
* Michael Clarkson, of Brooklyn, New York, who used the computer alias of "Barcode", was charged with possessing and trafficking in hardware used to obtain unauthorized access to telecommunications services.

The arrests and warrants flowing from the investigation mark the first successful law enforcement "sting" operation that identified persons allegedly involved in the intrusion of telecommunication computer networks, the computer theft of credit card account numbers and related personal information, and the theft of information used to obtain free cellular telephone service.[[Cybersnare Sting Arrest]]

[Cybersnare Sting Arrest]: https://fas.org/irp/news/1995/nj62_txt.html
[Operation Cybersnare]: https://en.wikipedia.org/wiki/Operation_Cybersnare
